<pre>
  ┌───────┐
  │       │
  │  a:o  │  acolono.com
  │       │
  └───────┘
</pre>

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Useful Links
 * Maintainers

INTRODUCTION
------------

LegalWeb Cloud integrates the service from https://www.legalweb.io

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/legalweb_cloud

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/legalweb_cloud?status=All&categories=All

REQUIREMENTS
------------

A subscription from https://www.legalweb.io


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.
   
 * Optionally activate Token (and Token Filter), to make use of the provided tokens.


CONFIGURATION
-------------

1. Set the GUIID

   This will need a subscription from https://www.legalweb.io

2. Execute Cron manually.

   This will save the remote configuration from legalweb.io.
   If the request has been successful, a Domain/Domain ID will be visible at the settings page.

3. Place your Imprint & Data Privacy Information in 3 different ways:

   1. Place Blocks (recommended)
   
      The Imprint & Data Privacy HTML Blobs are available as placeable Blocks.

   2. Use Token (depends on Token/Token Filter)
   
      In addition, [legalweb_cloud:imprint] as well as [legalweb_cloud:data_privacy] are filled with the same HTML Blobs.
      
      **Attention:** using "class" attribute must be allowed in your text format to make this step work.
 
   3. Activate Default Page

      This will activate 2 routes: /imprint and /data-privacy, under which each of the Blocks are rendered

In all three cases the appropriate HTML Blob for the current language will be used, if available.
If the HTML Blob in the current language is not available, the first language configured will be displayed.

USEFUL LINKS
------------

Details, Demo and more infos about the project and supported Systems
https://www.legalweb.io/


MAINTAINERS
-----------

Current maintainers:
 * Nikolay Grachev (granik) - https://www.drupal.org/u/granik
 * Nico Grienauer (Grienauer) - https://www.drupal.org/u/Grienauer
 * Alexander Sulz (vierlex) - https://www.drupal.org/u/vierlex


by acolono GmbH
---------------

~~we build your websites~~
we build your business

hello@acolono.com

www.acolono.com
www.twitter.com/acolono
www.drupal.org/acolono-gmbh
