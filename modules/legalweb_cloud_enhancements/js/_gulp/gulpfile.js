// lwc_enhancements gulp configuration.
const gulp   = require('gulp');
const babel  = require('gulp-babel');
const rename = require('gulp-rename');
const strip  = require('gulp-strip-comments');

// Build module's JS.
gulp.task('js', () => {
  return gulp.src('../es6/lwc_enhancements.es6.js')
    .pipe(strip())
    .pipe(
      babel({
        presets: [
          [
            "@babel/env",
            {
              modules: false,
            }
          ]
        ]
      })
    )
    .pipe(rename('lwc_enhancements.js'))
    .pipe(gulp.dest('..'));
});
