((Drupal) => {
  // LegalWeb Cloud Enhancements.
  const POPUP                    = '.lwb-pp, .sp-dsgvo-privacy-popup';
  const OVERLAY                  = '.lwb-pp-overlay, .sp-dsgvo-popup-overlay';
  const OVERLAY_CLASSNAME        = 'lwb-pp-overlay';
  const OVERLAY_LEGACY_CLASSNAME = 'sp-dsgvo-popup-overlay';
  const BUTTON_ALL               = '.lw-pp-btn-accept-all, .sp-dsgvo-popup-button';
  const BUTTON_SELECTION         = '.lw-pp-btn-accept-selection, .sp-dsgvo-privacy-btn-accept-selection';
  const BUTTON_NOTHING           = '.lw-pp-btn-accept-nothing, .sp-dsgvo-privacy-btn-accept-nothing';
  const BUTTON_CLOSE             = '.lwb-pp-close, .sp-dsgvo-popup-close';
  const CHECKBOX                 = '.switch-input:not(.switch-input-category)';
  const CATEGORY                 = '.switch-input-category';
  const CHECKBOX_CHECKED         = 'input.switch-input:not(.switch-input-category):checked';

  Drupal.behaviors.legalwebCloudEnhancements = {
    attach(context, settings)  {
      this.overlay = context.querySelector(OVERLAY);
      if (!this.overlay) {
        return;
      }
      // Initialize.
      this.initValues();

      if (settings.legalwebCloudEnhancements.adjustButtonLabels) {
        this.adjustButtonLabels();
        // Initial selection check.
        this._checkSelection();
        // Add event listeners.
        [].forEach.call(this.checkboxes, checkbox => {
          checkbox.addEventListener('change', this._checkSelection.bind(this));
        });
        [].forEach.call(this.categories, category => {
          category.addEventListener('change', this._checkCategorySelection.bind(this));
        });
      }

      if (settings.legalwebCloudEnhancements.removeCloseButton) {
        this.removeCloseButton();
      }

      if (settings.legalwebCloudEnhancements.disableOutsideClick) {
        this.disableOutsideClickBehavior();
      }
    },
    initValues() {
      this.popup = this.overlay.querySelector(POPUP);
      this.buttons = {};
      this.buttons.all = this.popup.querySelectorAll(BUTTON_ALL);
      this.buttons.acceptSelection = this.popup.querySelectorAll(BUTTON_SELECTION);
      this.buttons.acceptNothing = this.popup.querySelectorAll(BUTTON_NOTHING);
      this.checkboxes = this.popup.querySelectorAll(CHECKBOX);
      this.categories = this.popup.querySelectorAll(CATEGORY);
    },
    disableOutsideClickBehavior() {
      window.addEventListener('click', this._clickOutHandler, { capture: true });
      window.addEventListener('touchstart', this._clickOutHandler, { capture: true });
    },
    removeCloseButton() {
      const closeBtn = this.popup.querySelector(BUTTON_CLOSE);
      if (null !== closeBtn) {
        closeBtn.remove();
      }
    },
    adjustButtonLabels() {
      // Fix the layout.
      [].forEach.call(this.buttons.all, btn => {
        btn.style.minWidth = '40%';
        btn.style.textAlign = 'center';
      });
      // Save button labels.
      this.labels = {
        selected: this.buttons.acceptSelection[0].textContent,
        nothing: this.buttons.acceptNothing[0].textContent,
      };
      // Remove "Accept Nothing" button.
      [].forEach.call(this.buttons.acceptNothing, btn => {
        btn.remove();
      });
    },

    isNoneChecked() {
      return this.popup
        .querySelectorAll(CHECKBOX_CHECKED)
        .length === 0;
    },

    _clickOutHandler(e) {
      if (e.target.classList.contains(OVERLAY_CLASSNAME) || e.target.classList.contains(OVERLAY_LEGACY_CLASSNAME)) {
        e.stopPropagation();
        return false;
      }
    },

    _checkSelection() {
      if (this.isNoneChecked()) {
        // None checked.
        [].forEach.call(this.buttons.acceptSelection, btn => {
          btn.textContent = this.labels.nothing;
        });
      } else {
        // Some checked.
        [].forEach.call(this.buttons.acceptSelection, btn => {
          btn.textContent = this.labels.selected;
        });
      }
    },

    _checkCategorySelection(e) {
      setTimeout(() => {
        if (e.target.checked) {
          // Category checked.
          [].forEach.call(this.buttons.acceptSelection, btn => {
            btn.textContent = this.labels.selected;
          });
        }
        else if (this.isNoneChecked()) {
          // Category not checked.
          [].forEach.call(this.buttons.acceptSelection, btn => {
            btn.textContent = this.labels.nothing;
          });
        }
      }, 100);
    }
  };

})(Drupal);
