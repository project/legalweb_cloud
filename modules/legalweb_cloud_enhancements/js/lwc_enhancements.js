(function (Drupal) {
  var POPUP = '.lwb-pp, .sp-dsgvo-privacy-popup';
  var OVERLAY = '.lwb-pp-overlay, .sp-dsgvo-popup-overlay';
  var OVERLAY_CLASSNAME = 'lwb-pp-overlay';
  var OVERLAY_LEGACY_CLASSNAME = 'sp-dsgvo-popup-overlay';
  var BUTTON_ALL = '.lw-pp-btn-accept-all, .sp-dsgvo-popup-button';
  var BUTTON_SELECTION = '.lw-pp-btn-accept-selection, .sp-dsgvo-privacy-btn-accept-selection';
  var BUTTON_NOTHING = '.lw-pp-btn-accept-nothing, .sp-dsgvo-privacy-btn-accept-nothing';
  var BUTTON_CLOSE = '.lwb-pp-close, .sp-dsgvo-popup-close';
  var CHECKBOX = '.switch-input:not(.switch-input-category)';
  var CATEGORY = '.switch-input-category';
  var CHECKBOX_CHECKED = 'input.switch-input:not(.switch-input-category):checked';
  Drupal.behaviors.legalwebCloudEnhancements = {
    attach: function attach(context, settings) {
      var _this = this;
      this.overlay = context.querySelector(OVERLAY);
      if (!this.overlay) {
        return;
      }
      this.initValues();
      if (settings.legalwebCloudEnhancements.adjustButtonLabels) {
        this.adjustButtonLabels();
        this._checkSelection();
        [].forEach.call(this.checkboxes, function (checkbox) {
          checkbox.addEventListener('change', _this._checkSelection.bind(_this));
        });
        [].forEach.call(this.categories, function (category) {
          category.addEventListener('change', _this._checkCategorySelection.bind(_this));
        });
      }
      if (settings.legalwebCloudEnhancements.removeCloseButton) {
        this.removeCloseButton();
      }
      if (settings.legalwebCloudEnhancements.disableOutsideClick) {
        this.disableOutsideClickBehavior();
      }
    },
    initValues: function initValues() {
      this.popup = this.overlay.querySelector(POPUP);
      this.buttons = {};
      this.buttons.all = this.popup.querySelectorAll(BUTTON_ALL);
      this.buttons.acceptSelection = this.popup.querySelectorAll(BUTTON_SELECTION);
      this.buttons.acceptNothing = this.popup.querySelectorAll(BUTTON_NOTHING);
      this.checkboxes = this.popup.querySelectorAll(CHECKBOX);
      this.categories = this.popup.querySelectorAll(CATEGORY);
    },
    disableOutsideClickBehavior: function disableOutsideClickBehavior() {
      window.addEventListener('click', this._clickOutHandler, {
        capture: true
      });
      window.addEventListener('touchstart', this._clickOutHandler, {
        capture: true
      });
    },
    removeCloseButton: function removeCloseButton() {
      var closeBtn = this.popup.querySelector(BUTTON_CLOSE);
      if (null !== closeBtn) {
        closeBtn.remove();
      }
    },
    adjustButtonLabels: function adjustButtonLabels() {
      [].forEach.call(this.buttons.all, function (btn) {
        btn.style.minWidth = '40%';
        btn.style.textAlign = 'center';
      });
      this.labels = {
        selected: this.buttons.acceptSelection[0].textContent,
        nothing: this.buttons.acceptNothing[0].textContent
      };
      [].forEach.call(this.buttons.acceptNothing, function (btn) {
        btn.remove();
      });
    },
    isNoneChecked: function isNoneChecked() {
      return this.popup.querySelectorAll(CHECKBOX_CHECKED).length === 0;
    },
    _clickOutHandler: function _clickOutHandler(e) {
      if (e.target.classList.contains(OVERLAY_CLASSNAME) || e.target.classList.contains(OVERLAY_LEGACY_CLASSNAME)) {
        e.stopPropagation();
        return false;
      }
    },
    _checkSelection: function _checkSelection() {
      var _this2 = this;
      if (this.isNoneChecked()) {
        [].forEach.call(this.buttons.acceptSelection, function (btn) {
          btn.textContent = _this2.labels.nothing;
        });
      } else {
        [].forEach.call(this.buttons.acceptSelection, function (btn) {
          btn.textContent = _this2.labels.selected;
        });
      }
    },
    _checkCategorySelection: function _checkCategorySelection(e) {
      var _this3 = this;
      setTimeout(function () {
        if (e.target.checked) {
          [].forEach.call(_this3.buttons.acceptSelection, function (btn) {
            btn.textContent = _this3.labels.selected;
          });
        } else if (_this3.isNoneChecked()) {
          [].forEach.call(_this3.buttons.acceptSelection, function (btn) {
            btn.textContent = _this3.labels.nothing;
          });
        }
      }, 100);
    }
  };
})(Drupal);