<?php

namespace Drupal\legalweb_cloud_enhancements\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\legalweb_cloud\Form\SettingsForm;

/**
 * Contains LWCEnhancementsSettings form class.
 *
 * @package Drupal\legalweb_cloud_enhancements\Form
 */
class LWCEnhancementsSettings extends SettingsForm {

  const SETTINGS = 'legalweb_cloud_enhancements.settings';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config(static::SETTINGS);

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => t('Advanced Settings'),
      '#description' => t('By activating the following features the module adds its own JS to override the default Legalweb Cloud widget behavior.'),
      '#open' => FALSE,
    ];

    $form['advanced']['disable_outside_click'] = [
      '#type' => 'checkbox',
      '#title' => $this
        ->t('Disable outside click behavior'),
      '#description' => $this
        ->t('Click outside the popup-window will not close the dialog'),
      '#default_value' => $config->get('disable_outside_click'),
    ];

    $form['advanced']['remove_close_button'] = [
      '#type' => 'checkbox',
      '#title' => $this
        ->t('Remove "close" button from popup'),
      '#default_value' => $config->get('remove_close_button'),
    ];

    $form['advanced']['adjust_button_labels'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Adjust button labels depending on selected options'),
      '#description' => $this
        ->t('If some options are selected, the label will be "Accept selection". If none selected "Accept nothing" will be showed.'),
      '#default_value' => $config->get('adjust_button_labels'),
    ];

    $form['advanced']['attach_js_to_head'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load LegalWeb JS file in HTML head.'),
      '#description' => $this
        ->t("Use this function e.g if LegalWeb content blocker is not working with default settings.<br>
            By default LegalWeb JS file is loaded at the end of HTML body. <br>
            <strong>Cache clear required.</strong>"),
      '#default_value' => $config->get('attach_js_to_head'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config(static::SETTINGS)
      ->set('disable_outside_click', (bool) $form_state->getValue('disable_outside_click'))
      ->set('remove_close_button', (bool) $form_state->getValue('remove_close_button'))
      ->set('adjust_button_labels', (bool) $form_state->getValue('adjust_button_labels'))
      ->set('attach_js_to_head', (bool) $form_state->getValue('attach_js_to_head'))
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return array_merge(
      parent::getEditableConfigNames(),
      [
        static::SETTINGS,
      ]
    );
  }

}
