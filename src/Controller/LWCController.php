<?php

namespace Drupal\legalweb_cloud\Controller;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\legalweb_cloud\LWCManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * LWC local controller.
 */
class LWCController extends ControllerBase {

  /**
   * LWC Manager service.
   *
   * @var \Drupal\legalweb_cloud\LWCManager
   */
  protected $manager;

  /**
   * Block manager service.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\legalweb_cloud\LWCManager $manager
   *   LWC manager service.
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   Block manager service.
   */
  public function __construct(LWCManager $manager, BlockManagerInterface $block_manager) {
    $this->manager = $manager;
    $this->blockManager = $block_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('legalweb_cloud.manager'),
      $container->get('plugin.manager.block')
    );
  }

  /**
   * Renders default Imprint page, if active.
   *
   * @return array
   *   Block's render array.
   */
  public function imprintBlockPage() {
    return $this->renderBlock('legalweb_cloud_imprint_block');
  }

  /**
   * Renders default Data Privacy Page, if active.
   *
   * @return array
   *   Render array.
   */
  public function dataPrivacyBlockPage() {
    return $this->renderBlock('legalweb_cloud_data_privacy_block');
  }

  /**
   * Gte rendered version of first instance of block_plugin_id encountered.
   *
   * @param mixed $block_plugin_id
   *   Plugin id.
   *
   * @return array
   *   Render array.
   */
  private function renderBlock($block_plugin_id) {
    $block = $this->blockManager->createInstance($block_plugin_id, ['label_display' => '0']);
    return $block->build();
  }

}
