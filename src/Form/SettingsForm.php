<?php

namespace Drupal\legalweb_cloud\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\ProxyClass\Routing\RouteBuilder;
use Drupal\legalweb_cloud\LWCManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class module SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Managing class for rebuilding the router table.
   *
   * @var \Drupal\Core\ProxyClass\Routing\RouteBuilder
   */
  protected $routeBuilder;

  /**
   * LegalWebCloud manager service.
   *
   * @var \Drupal\legalweb_cloud\LWCManager
   */
  protected $manager;

  /**
   * SettingsForm class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\ProxyClass\Routing\RouteBuilder $route_builder
   *   Managing class for rebuilding the router table.
   * @param \Drupal\legalweb_cloud\LWCManager $manager
   *   LegalWebCloud manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RouteBuilder $route_builder, LWCManager $manager) {
    parent::__construct($config_factory);
    $this->routeBuilder = $route_builder;
    $this->manager = $manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.builder'),
      $container->get('legalweb_cloud.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      LWCManager::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'legalweb_cloud_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(LWCManager::SETTINGS);

    // Info from readme.
    $form['configuration'] = [
      '#markup' => "
        <details id='open-readme' class='js-form-wrapper form-wrapper seven-details'>
        <summary class='seve-details__summary' role='button' aria-controls='open-readme' aria-expanded='true' aria-pressed='true'>
          How to configure
        </summary>
        <div class='seven-details__wrapper details-wrapper'>
        <ol>
          <li><b>Set the GUIID</b><br>
          this will need a subscription from <a>https://www.legalweb.io</a></li>
      
          <li><b>Execute Cron manually</b><br>
           This will save the remote configuration from legalweb.io.
           If the request has been successful, a Domain/Domain ID will be visible at the settings page.</li>
        
          <li>Place your Imprint & Data Privacy Information in 3 different ways:</li>
            <ol>
              <li><b>Place Blocks (recommended)</b><br>
              The Imprint & Data Privacy HTML Blobs are available as placeable Blocks.</li>
        
              <li>Use Token (depends on Token/Token Filter)<br>
              In addition, <b>[legalweb_cloud:imprint]</b> as well as <b>[legalweb_cloud:data_privacy]</b> are filled with the same HTML Blobs.</li>
         
              <li><b>Activate Default Page</b><br>
              This will activate 2 routes: /imprint and /data-privacy, under which each of the Blocks are rendered</li>
          </ol>
        </ol>
        In all three cases the appropriate HTML Blob for the current language will be used, if available.
        If the HTML Blob in the current language is not available, the first language configured will be displayed.
        </div>
        </details>",
    ];

    // General Info.
    $form['guid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GUID'),
      '#maxlength' => 256,
      '#size' => 64,
      '#default_value' => $config->get('guid'),
      '#description' => $this->t('A subscription to a package from <a>https://legalweb.io/</a> is needed.'),
    ];
    // Only for feedback.
    $form['domain_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain ID'),
      '#description' => $this->t('Value is configured/set via legalweb.io'),
      '#maxlength' => 256,
      '#size' => 64,
      '#default_value' => $this->manager->getDomainId(),
      '#disabled' => TRUE,
    ];
    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#description' => $this->t('Value is configured/set via legalweb.io'),
      '#maxlength' => 256,
      '#size' => 64,
      '#default_value' => $this->manager->getDomain(),
      '#disabled' => TRUE,
    ];

    $form['callback'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'lwc-callback',
        ],
      ],
    ];

    $form['callback']['callback_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Callback URL'),
      '#description' => $this->t("Visiting this URL will force a API request to legalweb.io.<br>
Use this if you don't want to use the build in update via cron.<br>This URL is generated on installing the Plugin and should not be changed."),
      '#maxlength' => 256,
      '#size' => 64,
      '#default_value' => $this->manager->getCallbackUrl(),
      '#disabled' => TRUE,
      '#attributes' => [
        'class' => [
          'lwc-callback__url',
        ],
      ],
    ];

    $form['callback']['callback_btn'] = [
      '#type' => 'markup',
      '#markup' => '<div class="lwc-callback__link"><a href="' . $this->manager->getCallbackUrl()
      . '" class="button button--secondary" target="_blank">'
      . $this->t('Execute callback')
      . '</a></div>',
    ];

    // All about updating.
    $form['cron'] = [
      '#type' => 'checkbox',
      '#title' => $this
        ->t('Activate Automatic Update'),
      '#description' => $this
        ->t('Get the updated settings from Legalweb automatically every 24h.'),
      '#default_value' => $config->get('cron'),
    ];

    // Anaging the Impring & Data Privacy Pages, as well as Links to them.
    // @todo if default pages checkboxes are NOT checked AND if there are
    //   no instances blocks of DataPrivacyBlock and ImprintBlock,
    //   show warning that the module can't check for [legalweb_cloud] tokens,
    //   if they are used and that a site should have said pages.
    $form['data_privacy_page'] = [
      '#type' => 'checkbox',
      '#title' => $this
        ->t('Default Data Privacy Page'),
      '#description' => $this
        ->t('This will activate the build-in route for the Data Privacy Page,
        reachable at /data-privacy.'),
      '#default_value' => $config->get('data_privacy_page'),
    ];
    $form['imprint_page'] = [
      '#type' => 'checkbox',
      '#title' => $this
        ->t('Default Imprint Page'),
      '#description' => $this
        ->t('This will activate the build-in route for the Imprint Page,
        reachable at /imprint'),
      '#default_value' => $config->get('imprint_page'),
    ];

    $form['#attached']['library'] = ['legalweb_cloud/backend'];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config(LWCManager::SETTINGS)
      ->set('guid', $form_state->getValue('guid'))
      ->set('cron', $form_state->getValue('cron'))
      ->set('data_privacy_page', $form_state->getValue('data_privacy_page'))
      ->set('imprint_page', $form_state->getValue('imprint_page'))
      ->save();

    $this->routeBuilder->rebuild();
  }

}
