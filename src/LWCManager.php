<?php

namespace Drupal\legalweb_cloud;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use GuzzleHttp\Exception\GuzzleException;

/**
 * LegalWeb Cloud Manager service.
 */
class LWCManager {

  const SETTINGS = 'legalweb_cloud.settings';
  const API_URL = 'https://legalweb.io/api';
  const RESPONSE_PATH = 'private://legalweb_cloud/';
  const RESPONSE_FILENAME = 'response.json';
  const ASSET_PATH = 'public://legalweb_cloud/';
  const ASSET_FILENAME = 'legalweb_cloud';

  /**
   * Callback URL to load LWC assets.
   *
   * @var string
   */
  protected $callbackUrl = '';

  /**
   * LWC remote settings.
   *
   * @var array
   */
  protected $lwcSettings = [];

  /**
   * LWC module settings.
   *
   * @var array
   */
  protected $settings;

  /**
   * Http client factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpFactory;

  /**
   * Drupal state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * File System service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * LWCManager class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   HTTP Client factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   Drupal state service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File system service.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              ClientFactory $http_client_factory,
                              StateInterface $state,
                              FileSystemInterface $file_system) {
    $this->settings = $config_factory->get(LWCManager::SETTINGS);
    $this->httpFactory = $http_client_factory;
    $this->state = $state;
    $this->fileSystem = $file_system;
    $this->getRemoteSettings();
  }

  /**
   * Gets the Legalweb.io Config and saves it in the private folder.
   *
   * @param bool $force
   *   Do not take the last update check into account.
   *
   * @return bool
   *   TRUE in case of success.
   */
  public function requestRemoteSettings(bool $force = FALSE) {
    $now = time();
    if (!$force) {
      // Check State API when the last update request was less than 24h ago.
      // Skip if $force = TRUE.
      $interval = $this->settings->get('interval') ?? 86400;
      $last_update = $this->state->get('legalweb_cloud.last_update') ?? 0;

      if (($now - $last_update) < $interval) {
        return FALSE;
      }
    }

    $http_client = $this->httpFactory->fromOptions([
      'base_uri' => self::API_URL,
      'verify' => TRUE,
      'timeout' => 30,
      'headers' => [
        'content-type' => 'application/x-www-form-urlencoded',
        'Guid' => $this->settings->get('guid'),
        'Callback' => $this->getCallbackUrl(),
      ],
    ]);

    try {
      $response = $http_client->request('GET');
    }
    catch (GuzzleException $e) {
      // @todo log request error.
      return FALSE;
    }

    $response_json = $response->getBody();

    // Request is invalid if a code is returned.
    if (!empty($response_json->code)) {
      // @todo log ->code and ->message.
      return FALSE;
    }

    $response_abs_path = self::RESPONSE_PATH . self::RESPONSE_FILENAME;
    if (file_exists($response_abs_path)) {
      $this->fileSystem->copy($response_abs_path,
        $response_abs_path . '.old',
        FileSystemInterface::EXISTS_REPLACE);
    }

    $directory = self::RESPONSE_PATH;
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    $this->fileSystem->saveData(
      $response_json,
      $response_abs_path,
      FileSystemInterface::EXISTS_REPLACE
    );

    $this->state->set('legalweb_cloud.last_update', $now);

    $this->generateAssets(
      json_decode($response_json)
    );

    return TRUE;
  }

  /**
   * Callback URL for legalweb.io to force an API Call from their side.
   *
   * @return array
   *   Empty array.
   */
  public function callbackConfiguration() {
    $this->requestRemoteSettings(TRUE);
    return [];
  }

  /**
   * Separates the response.json into different files (libraries).
   *
   * @param array|null $response
   *   API response data.
   *
   * @return bool
   *   TRUE in case of success, FALSE otherwise.
   */
  public function generateAssets($response = NULL) {
    // If no response data is passed, read from current reponse.json
    // in the local file system.
    if (empty($response)) {
      $response_abs_path = self::RESPONSE_PATH . self::RESPONSE_FILENAME;
      if (file_exists($response_abs_path)) {
        $response = json_decode(file_get_contents($response_abs_path));
      }
      else {
        // @todo log error.
        return FALSE;
      }
    }

    /*
     * Generate CSS file in the public files folder
     * @todo check if anything has changed -> trigger cache something
     */
    $asset_directory = self::ASSET_PATH;
    $this->fileSystem->prepareDirectory($asset_directory, FileSystemInterface::CREATE_DIRECTORY);
    $this->fileSystem->saveData(
      $response->services->dppopupcss,
      $asset_directory . self::ASSET_FILENAME . '.css',
      FileSystemInterface::EXISTS_REPLACE
    );

    /*
     * Generate JS file in the public files folder
     * Encapsulate in Drupal Behaviour
     * @todo check if anything has changed -> trigger cache something
     */
    $legalweb_cloud_js = $response->services->dppopupjs;
    $general_config = json_encode($response->services->dppopupconfig->spDsgvoGeneralConfig);
    $integration_config = json_encode($response->services->dppopupconfig->spDsgvoIntegrationConfig);
    $js = "
      var spDsgvoGeneralConfig = JSON.parse('$general_config');
      var spDsgvoIntegrationConfig = JSON.parse('$integration_config');

      $legalweb_cloud_js
    ";
    $this->fileSystem->saveData(
      $js,
      $asset_directory . self::ASSET_FILENAME . '.js',
      FileSystemInterface::EXISTS_REPLACE
    );

    /*
     * Generate the $this->remote_config as a file.
     * It is a slimmed down version of response.json, without the CSS & JS
     * from above.
     * @todo check if anything has changed -> trigger cache something
     */
    unset($response->services->dppopupjs,
      $response->services->dppopupcss,
      $response->services->dppopupconfig->spDsgvoGeneralConfig,
      $response->services->dppopupconfig->spDsgvoIntegrationConfig);
    $legalweb_cloud_settings = json_encode($response);
    $this->fileSystem->saveData(
      $legalweb_cloud_settings,
      $asset_directory . self::ASSET_FILENAME . '.json',
      FileSystemInterface::EXISTS_REPLACE
    );

    // @todo possible to delete only those for the module?
    _drupal_flush_css_js();

    return TRUE;
  }

  /**
   * Returns all or the value of given key from Legalweb s JSON.
   *
   * @return array|null
   *   Array of settings.
   */
  public function getRemoteSettings() {
    if (!empty($this->lwcSettings)) {
      return $this->lwcSettings;
    }

    $legalweb_cloud_settings_path = self::ASSET_PATH . self::ASSET_FILENAME . '.json';
    if (file_exists($legalweb_cloud_settings_path)) {
      $this->lwcSettings = json_decode(
        file_get_contents($legalweb_cloud_settings_path)
      );
    }

    return $this->lwcSettings;
  }

  /**
   * Generate Callback URL.
   *
   * @return string
   *   Callback Url.
   */
  public function getCallbackUrl() {
    if (empty($this->callbackUrl)) {
      try {
        $url = Url::fromRoute('legalweb_cloud.callback_page', [], ['absolute' => TRUE]);
        $this->callbackUrl = $url->toString();
      }
      catch (\Exception $exception) {
        // Happens during install once.
      }
    }
    return $this->callbackUrl;
  }

  /**
   * Current mappings for the remote configuration.
   */
  public function getDomainId() {
    return $this->lwcSettings->domain->domain_id ?? NULL;
  }

  /**
   * Get current website's domain.
   *
   * @return string|null
   *   Domain or empty.
   */
  public function getDomain() {
    return $this->lwcSettings->domain->domain_url ?? NULL;
  }

  /**
   * Get imprint text as HTML.
   */
  public function getImprintHtml($langcode = NULL) {
    return $this->getServiceHtml('imprint', $langcode);
  }

  /**
   * Get data privacy text as HTML.
   */
  public function getDataPrivacyHtml($langcode = NULL) {
    return $this->getServiceHtml('dpstatement', $langcode);
  }

  /**
   * Get popup HTML code.
   */
  public function getPopupHtml($langcode = NULL) {
    return $this->getServiceHtml('dppopup', $langcode);
  }

  /**
   * Get HTML code for defined service.
   */
  protected function getServiceHtml($service, $langcode = NULL) {
    $html = NULL;
    if ($langcode &&
      !empty($this->lwcSettings->services->$service)) {
      $html = $this->lwcSettings->services->$service->$langcode ?? NULL;
    }

    if (!$html && !empty($this->lwcSettings->services->$service)) {
      foreach ($this->lwcSettings->services->$service as $key => $value) {
        $html = $value;
        break;
      }
    }

    return $html;
  }

}
