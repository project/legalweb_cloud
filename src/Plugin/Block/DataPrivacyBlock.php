<?php

namespace Drupal\legalweb_cloud\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'DataPrivacyBlock' block.
 *
 * @Block(
 *  id = "legalweb_cloud_data_privacy_block",
 *  admin_label = @Translation("Data Privacy"),
 * )
 */
class DataPrivacyBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * LegalWeb Cloud Manager.
   *
   * @var \Drupal\legalweb_cloud\LWCManager
   */
  protected $manager;

  /**
   * Language Manager Interface.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->manager = $container->get('legalweb_cloud.manager');
    $instance->languageManager = $container->get('language_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    $data_privacy = Markup::create($this->manager->getDataPrivacyHTML($langcode)) ??
      'Data Privacy has not been configured properly at legalweb.io, please check the Documentation of this module.';

    // @todo set cache tags/context here?
    return [
      '#theme' => 'legalweb_cloud_data_privacy',
      '#content' => $data_privacy,
    ];
  }

}
