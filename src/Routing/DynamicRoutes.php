<?php

namespace Drupal\legalweb_cloud\Routing;

use Drupal\Component\Utility\Random;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\legalweb_cloud\LWCManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * DynamicRoutesAlter service class.
 */
class DynamicRoutes implements ContainerInjectionInterface {

  /**
   * LWC Module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * LWC Module settings editable.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $configEditable;

  /**
   * DynamicBlockPages constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get(LWCManager::SETTINGS);
    $this->configEditable = $config_factory->getEditable(LWCManager::SETTINGS);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $collection = new RouteCollection();

    /*
     * @todo how are multilang routes generated?
     */

    if ($this->config->get('imprint_page')) {
      $imprint = new Route(
        '/imprint',
        [
          // @todo delegate title  to callback of controller function.
          '_title' => 'Imprint',
          '_controller' => '\Drupal\legalweb_cloud\Controller\LWCController::imprintBlockPage',
        ],
        [
          '_access' => 'TRUE',
        ]
      );
      $collection->add('legalweb_cloud.imprint_page', $imprint);
    }

    if ($this->config->get('data_privacy_page')) {
      $data_privacy = new Route(
        '/data-privacy',
        [
          // @todo delegate title  to callback of controller function.
          '_title' => 'Data Privacy',
          '_controller' => '\Drupal\legalweb_cloud\Controller\LWCController::dataPrivacyBlockPage',
        ],
        [
          '_access' => 'TRUE',
        ]
      );
      $collection->add('legalweb_cloud.data_privacy_page', $data_privacy);
    }

    $callback_path = $this->config->get('callback_path') ?? NULL;
    if (empty($callback_path)) {
      $random = new Random();
      $callback_path = '/legalweb_cloud/callback/' . $random->word(24);
      $this->configEditable->set('callback_path', $callback_path)
        ->save();
    }
    $callback = new Route(
      $callback_path,
      [
        '_title' => 'Welcome back LegalWeb Cloud!',
        '_controller' => 'legalweb_cloud.manager:callbackConfiguration',
      ],
      [
        '_access' => 'TRUE',
      ]
    );
    $collection->add('legalweb_cloud.callback_page', $callback);

    return $collection;
  }

}
